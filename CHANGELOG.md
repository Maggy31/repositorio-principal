Changelog
modifico archivo en la rama hu-tarea-25

Todos los cambios notables a este proyecto serán documentados es este archivo.

El formato esta basado en [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), mantenemos las versiones de acuerdo
a [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

**Dada una versión número MAJOR.MINOR.PATCH:** (aplica a partir de 2.0.0)

* Se incrementará **MAJOR**, cuando se implemente un nuevo tipo de **Módulo** que genere incompatibilidad con versión
  anterior.
* Se incrementa **MINOR**, cuando se agregue una o más **nueva(s) funcionalidad(es) a la aplicación**.
* Se incrementa **PATCH**, cuando se implemente un **fix**.

## [Released]

## [0.1.0] - 2022-02-08
### [add]
* [HU-LINEA-05](https://openproject.infotec.mx/projects/aamates-desarrollo/) - Historia de Usuario 5

## [0.0.2] - 2022-02-04

### [add]

* [HU-LINEA-07](https://openproject.infotec.mx/projects/aamates-desarrollo/) - Se crean directorios para flujo de
  trabajo de CI/CD.